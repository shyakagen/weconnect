
<?php
session_start();
error_reporting();
if(!isset($_SESSION['username'])){ 
    header ("Location: login.php"); 
    exit;
}
//DATABASE CONNECTION

include('config.php');

//check if the form has been sumbited
$msg="";
$error="";
$credidential_id ="";

if(isset($_POST['upload'])){


    //SET VARIABLE

    $credidential_id        =$_POST['credidential_id'];
    $tin_number             =$_POST['tin_number'];
    $business_name          =$_POST['business_name'];
    $business_category      =$_POST['business_category'];
    $business_description   =$_POST['business_description'];
    $business_location      =$_POST['business_location'];
    $business_logo          =$_POST['business_logo'];
    $status                 = "Active";

    $menu                   ="Menu";
    $parking                ="Parking";
    $rooms                  ="Rooms";
    $swimmingpool           ="swimmingpool";
    $customercare           ="customercare";


    

    //CHECK IF THE TIN NUMBER HAS ALREADY EXIST

    $check = $conn->prepare("SELECT  tin_number FROM business WHERE  tin_number= :tin_number");
    $check->bindValue(':tin_number', $_POST['tin_number']);
    $check->execute();
    $result = $check->fetch();

    if($result){

        //LET MOVE OUR IMAGES

        move_uploaded_file($_FILES["business_logo"]["tmp_name"],"uploads/" . $_FILES["business_logo"]["name"]);         
        $business_logo=$_FILES["business_logo"]["name"];

        //LET DO OUR QUERY FOR UPDATING OUR DATA 

        $sql  = "UPDATE business SET business_category=:business_category,business_description=:business_description,business_location=:business_location,business_logo=:business_logo,credidential_id=:credidential_id,status=:status WHERE tin_number=:tin_number";

        //QUERY FOR INSERTING SUB CATEGORIES

        $sql1 = "INSERT INTO subcategorry(menu,parking,rooms,swimmingpool,customercare,credidential_id)VALUES(:menu,:parking,:rooms,:swimmingpool,:customercare,:credidential_id)";


        $query = $conn->prepare($sql);
        // $query->bindParam(':business_name',$business_name,PDO::PARAM_STR);
        $query->bindParam(':credidential_id',$credidential_id,PDO::PARAM_STR);
        $query->bindParam(':tin_number',$tin_number,PDO::PARAM_STR);
        $query->bindParam(':business_category',$business_category,PDO::PARAM_STR);
        $query->bindParam(':business_description',$business_description,PDO::PARAM_STR);
        $query->bindParam(':business_location',$business_location,PDO::PARAM_STR);
        $query->bindParam(':business_logo',$business_logo,PDO::PARAM_STR);
        $query->bindParam(':status',$status,PDO::PARAM_STR);

        $query1 = $conn->prepare($sql1);
        $query1->bindParam(':menu',$menu,PDO::PARAM_STR);
        $query1->bindParam(':parking',$parking,PDO::PARAM_STR);
        $query1->bindParam(':rooms',$rooms,PDO::PARAM_STR);
        $query1->bindParam(':swimmingpool',$swimmingpool,PDO::PARAM_STR);
        $query1->bindParam(':customercare',$customercare,PDO::PARAM_STR);
        $query1->bindParam(':credidential_id',$credidential_id,PDO::PARAM_STR);


        $query1->execute();
        $query->execute();

        $msg= "Business Successful Created";
        header('Location:owner_view_business.php');
    }else{
        $error = "Dear Friend Please Register Your Business in RDB";
    }
}
?>
<!doctype html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Weconnect</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="icon" href="logo.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/gijgo.min.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/all.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="css/style.css">

    <link rel="stylesheet" type="text/css" href="css/styless.css" media="all" />
    <link rel="stylesheet" type="text/css" href="css/demo.css" media="all" />
    <!-- jQuery -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>

    <!-- FlexSlider -->
    <script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" charset="utf-8">
        var $ = jQuery.noConflict();
        $(window).load(function() {
            $('.flexslider').flexslider({
              animation: "fade"
          });

            $(function() {
                $('.show_menu').click(function(){
                    $('.menu').fadeIn();
                    $('.show_menu').fadeOut();
                    $('.hide_menu').fadeIn();
                });
                $('.hide_menu').click(function(){
                    $('.menu').fadeOut();
                    $('.show_menu').fadeIn();
                    $('.hide_menu').fadeOut();
                });
            });
        });
    </script>
</head>
<body>
   <?php include('owner_header.php');?>
   <!-- Header part end-->
   <!--================Blog Area =================-->
   <section class="blog_area section_padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="blog_right_sidebar">
                    <aside class="single_sidebar_widget search_widget">
                        <?php if($msg){?>
                            <div class="alert alert-success left-icon-alert" role="alert">
                               <strong>Well done!</strong><?php echo htmlentities($msg); ?>
                               </div><?php } 
                               else if($error){?>
                                <div class="alert alert-danger left-icon-alert" role="alert">
                                    <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                                </div>
                            <?php } ?>
                            <p align="center"><b>Business Registration Form</b></p>

                            <form method="POST" action="<?php echo $_SERVER['PHP_SELF'];?>"  enctype="multipart/form-data">
                                <?php 
                                      // DB CREDENTIALS
                              
                             //ESTABLISH DATABASE CONNECTION

                                try{
                                  $conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME,DB_USER, DB_PASS,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
                              }
                              catch(PDOException $e){
                                  exit("Error:". $e->getMessage());
                              }
                              // $credidential_id ="";
                              $credidential_id = $_GET['credidential_id'];
                              $sql = "SELECT * FROM credidential WHERE credidential_id='".$_SESSION['credidential_id']."'";                           
                              $query = $conn->prepare($sql);
                              $query->bindParam(':credidential_id',$credidential_id,PDO::PARAM_STR);
                              $query->execute();
                              $results=$query->fetchAll(PDO::FETCH_OBJ);
                              $cnt=1;
                              if($query->rowCount() > 0)
                              {
                                  foreach($results as $result)
                                  {   
                                      ?>  
                                      <div class="form-group row">
                                        <label for="credidential_id" class="col-md-4 col-form-label text-md-right"></label>
                                        <div class="col-md-8">
                                            <input id="person_id" type="hidden" class="form-control " name="credidential_id" value="<?php echo htmlentities($result->credidential_id);?>" required autocomplete="credidential_id">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="tin_number" class="col-md-4 col-form-label text-md-right">Your Tin number Is</label>
                                        <div class="col-md-8">
                                            <input id="tin_number" type="text"  class="form-control " name="tin_number" value="" required autocomplete="off">
                                            
                                        </div>
                                    </div>

                                    <!-- <div class="form-group row">
                                        <label for="business_name" class="col-md-4 col-form-label text-md-right">Business name</label>

                                        <div class="col-md-8">
                                            <input id="business_name" type="text" class="form-control " name="business_name" value="" required autocomplete="off">
                                        </div>
                                    </div> -->
                                    

                                    <div class="form-group row">
                                        <label for="last_name" class="col-md-4 col-form-label text-md-right">Business Category</label>

                                        <div class="col-md-8">
                                           Hotel <input type="radio" class="col-md-4 col-form-label text-md-right" onclick="javascript:yesnoCheck();" value="Hotel" name="business_category" id="yesCheck">Restaurant<input type="radio" onclick="javascript:yesnoCheck();" class="col-md-4 col-form-label text-md-right" value="Restaurant" name="business_category" id="yesChecked"><br>
                                        </div>
                                    </div>

                                     <div id="ifYes" style="display:none">
                                        <div class="form-group row">
                                        <label for="business_name" class="col-md-4 col-form-label text-md-right">Menu</label>

                                        <div class="col-md-8">
                                            <input id="menu" type="text" class="form-control " name="menu" value="Menu" required readonly="">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="business_name" class="col-md-4 col-form-label text-md-right">Parking</label>

                                        <div class="col-md-8">
                                            <input id="parking" type="text" class="form-control " name="parking" value="Parking" readonly="">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="business_name" class="col-md-4 col-form-label text-md-right">Rooms</label>

                                        <div class="col-md-8">
                                            <input id="business_name" type="text" class="form-control " name="rooms" value="Rooms"readonly="">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="business_name" class="col-md-4 col-form-label text-md-right">SwimmingPool</label>

                                        <div class="col-md-8">
                                            <input id="business_name" type="text" class="form-control " name="swimmingpool" value="SwimmingPool"readonly="">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="business_name" class="col-md-4 col-form-label text-md-right">CustomerCare</label>

                                        <div class="col-md-8">
                                            <input id="business_name" type="text" class="form-control " name="customercare" value="CustomerCare"readonly="">
                                        </div>
                                    </div>
                           
                                    </div>


                                     <div id="ifYess" style="display:none">
                                        <div class="form-group row">
                                        <label for="business_name" class="col-md-4 col-form-label text-md-right">Menu</label>

                                        <div class="col-md-8">
                                            <input id="menu" type="text" class="form-control " name="menu" value="Menu" required readonly="">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="business_name" class="col-md-4 col-form-label text-md-right">Parking</label>

                                        <div class="col-md-8">
                                            <input id="parking" type="text" class="form-control " name="parking" value="Parking" readonly="">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="business_name" class="col-md-4 col-form-label text-md-right">SwimmingPool</label>

                                        <div class="col-md-8">
                                            <input id="business_name" type="text" class="form-control " name="swimmingpool" value="SwimmingPool"readonly="">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="business_name" class="col-md-4 col-form-label text-md-right">CustomerCare</label>

                                        <div class="col-md-8">
                                            <input id="business_name" type="text" class="form-control " name="customercare" value="CustomerCare"readonly="">
                                        </div>
                                    </div>
                           
                                    </div>



                                    <div class="form-group row">
                                        <label for="email_address" class="col-md-4 col-form-label text-md-right">Business Description</label>
                                        <div class="col-md-8">
                                            <textarea class="form-control " name="business_description" required=""></textarea>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label for="email" class="col-md-4 col-form-label text-md-right">BUSINESS LOGO</label>
                                        <div class="col-md-6">
                                           <input type="hidden" name="size" value="1000000" class="form-control" />
                                           <input id="" type="file" name="business_logo" class="form-control" />
                                       </div>
                                   </div>
                                   
                                   <div class="form-group row">
                                    <label for="telephone_number" class="col-md-4 col-form-label text-md-right">Business location</label>

                                    <div class="col-md-8">
                                        <input id="business_location" type="text" class="form-control " name="business_location" value="" required autocomplete="off">
                                    </div>
                                </div> 

                                <input class="button rounded-0 primary-bg text-white w-100 btn_4" type="hidden" value="Record" />
                                <input class="button rounded-0 primary-bg text-white w-100 btn_4" type="submit" name="upload" value="Register Business" />

                            </form>
                        <?php }} ?>
                        
                    </aside>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="blog_right_sidebar">
                    
                 <aside class="single_sidebar_widget post_category_widget">
                    <h4 class="widget_title">Our Business Category</h4>
                    <ul class="list cat-list">
                        <li>
                            <a href="#" class="d-flex">
                                <p>Resaurant Food</p>                         
                            </a>
                        </li>
                        <li>
                            <a href="#" class="d-flex">
                                <p>Hotels</p>
                            </a>
                        </li>
                       
                    </ul>
                </aside>
            </div>
        </div>
    </div>
</div>
</section>
<!--================Blog Area =================-->
<!-- footer part start-->
<?php include('footer.php');?>
<!-- footer part end-->
<!-- jquery plugins here-->
<!-- jquery -->
<script type="text/javascript">

function yesnoCheck() {
    if (document.getElementById('yesCheck').checked) {
        document.getElementById('ifYes').style.display = 'block';

    }else if(document.getElementById('yesChecked').checked){
         document.getElementById('ifYess').style.display = 'block';

    }   
    else  document.getElementById('ifYes').style.display = 'none';

}



</script>
<script src="js/jquery-1.12.1.min.js"></script>
<!-- popper js -->
<script src="js/popper.min.js"></script>
<!-- bootstrap js -->
<script src="js/bootstrap.min.js"></script>
<!-- easing js -->
<script src="js/jquery.magnific-popup.js"></script>
<!-- swiper js -->
<script src="js/swiper.min.js"></script>
<!-- swiper js -->
<script src="js/masonry.pkgd.js"></script>
<!-- particles js -->
<script src="js/owl.carousel.min.js"></script>
<!-- swiper js -->
<script src="js/slick.min.js"></script>
<script src="js/gijgo.min.js"></script>
<script src="js/jquery.nice-select.min.js"></script>
<!-- custom js -->
<script src="js/custom.js"></script>
</body>

</html>
