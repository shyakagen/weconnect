<footer class="footer-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-sm-6 col-md-4">
                <div class="single-footer-widget footer_1">
                    <h4>About Us</h4>
                    <p>Weconnect is a part of RDB Platform that allow the business onwer to create their Business on this platform to allow them to participate in Development of the country and in RDB ceremonies awards</p>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 col-md-4">
                <div class="single-footer-widget footer_2">
                    <h4>Important Link</h4>
                    <div class="contact_info">
                        <ul>
                            <li><a href="https://rdb.rw/">Rwanda Development Board</a></li>
                            <li><a href="https://www.rra.gov.rw/">Rwanda Revenue Authority</a></li>
                            <li><a href="https://localhost/weconnectemplate/">Weconnect</a></li>
                           
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6 col-md-4">
                <div class="single-footer-widget footer_2">
                    <h4>Contact us</h4>
                    <div class="contact_info">
                        <p><span> Address :</span>www.rdb.rw</p>
                        <p><span> Phone :</span> +0783484784</p>
                        <p><span> Email : </span>weconnect@gmail.com</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-sm-8 col-md-6">
                <div class="single-footer-widget footer_3">
                    <h4>Newsletter</h4>
                    <p>You can leave your Email address in order that you can get our daily notification</p>
                    <form action="#">
                        <div class="form-group">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder='Email Address'
                                onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address'">
                                <div class="input-group-append">
                                    <button class="btn" type="button"><i class="fas fa-paper-plane"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="copyright_part_text">
            <div class="row">
                <div class="col-lg-8">
                    <p class="footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;Shyaka Gen All rights reserved | This template is made with <i class="ti-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Esther UMUBYEYI</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                    </div>
                    <div class="col-lg-4">
                        <div class="copyright_social_icon text-right">
                            <a href="#"><i class="fab fa-facebook-f"></i></a>
                            <a href="#"><i class="fab fa-twitter"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>