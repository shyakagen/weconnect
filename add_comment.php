<?php
session_start();
error_reporting();
// DB CREDENTIALS
include('config.php');
//CHECK I THE FORM HAS BEEN SUBMITTED

if(isset($_POST['posted'])){

  //VARIABLE
  $business_id      = htmlentities($_POST['business_id']);
  $email_comment    = htmlentities($_POST['email_comment']);
  $username         = htmlentities($_POST['username']);
  $comment          = htmlentities($_POST['comment']);
  $comment_status   = htmlentities($_POST['comment_status']);
  $sub_categs       = htmlentities($_POST['sub_categs']);  
  $time_comment     = date("H:i:s");

  //SQL QUERY FOR SAVING

  $sql              =  "INSERT INTO comment(email_comment,username,comment,time_comment,comment_status,sub_categs,business_id)VALUES(:email_comment,:username,:comment,:time_comment,:comment_status,:sub_categs,:business_id)";
  $query            = $conn->prepare($sql);
  $query->bindParam(':email_comment',$email_comment,PDO::PARAM_STR);
  $query->bindParam(':username',$username,PDO::PARAM_STR);
  $query->bindParam(':comment',$comment,PDO::PARAM_STR);
  $query->bindParam(':time_comment',$time_comment,PDO::PARAM_STR);
  $query->bindParam(':comment_status',$comment_status,PDO::PARAM_STR);
  $query->bindParam(':business_id',$business_id,PDO::PARAM_STR);
   $query->bindParam(':sub_categs',$sub_categs,PDO::PARAM_STR);
  $query->execute();
  if($query){
   echo "<script>alert('Successfully Posted'); window.location = 'index.php';</script>";

  }else{
     echo "<script>alert('Failed To Posted'); window.location = 'comment.php';</script>";
  }
}

?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Weconnect</title>
    <link rel="icon" href="logo.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/gijgo.min.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/all.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <?php include('header_nav.php');?>
   <!--================Blog Area =================-->
   <section class="blog_area single-post-area section_padding">

      <div class="container">
         <div class="row">
            <div class="col-lg-8 posts-list">
              <?php
                   //connection DB 
                   include('config.php');
                   $business_id = $_GET['business_id'];
                   $select = $conn->prepare("SELECT * FROM business WHERE business_id=:business_id");
                   $select->bindParam(':business_id',$business_id,PDO::PARAM_STR);
                   $select->setFetchMode(PDO::FETCH_ASSOC);
                   $select->execute();
                   while($data=$select->fetch()){
                    ?>
               <div class="single-post">
                  <div class="feature-img">
                      <img src="uploads/<?php echo $data['business_logo'];?>">
                  </div>
               </div>
             </br>
               <div class="comment-form">
                <p>Our Listed Item Category</p>

                  <form method = "POST" class="form-contact comment_form" action="<?php echo $_SERVER['PHP_SELF'];?>" >
                     <?php 
                      // DB CREDENTIALS
                     include('config.php');
                 
                  $business_id = $_GET['business_id'];
                  $sql = "SELECT * FROM business WHERE business_id=:business_id";                              
                  $query = $conn->prepare($sql);
                  $query->bindParam(':business_id',$business_id,PDO::PARAM_STR);
                  $query->execute();
                  $results=$query->fetchAll(PDO::FETCH_OBJ);
                  $cnt=1;
                  if($query->rowCount() > 0)
                  {
                  foreach($results as $result)
                  {   
                      ?>  
                     <div class="row">

                      <div class="col-sm-12">
                           <div class="form-group">
                              <input class="form-control" name="business_id" id="business_id" value="<?php echo htmlentities($result->business_id);?>" type="hidden" placeholder="business_id">
                           </div>
                        </div>

                         <div class="col-sm-12">
                           <div class="form-group">
                              <select  class="form-control" name="sub_categs">
                              <option selected="selected">Item Category</option>
                              <option value="Menu">Menu</option>
                              <option value="Parking">Parking</option>
                              <option value="CustomerCare">CustomerCare</option>
                               <option value="Rooms">Rooms</option>
                          </select>
                           </div>
                        </div>

                    
                        <div class="col-sm-6">
                           <div class="form-group">
                             <input class="form-control" name="email_comment" type="email" placeholder="Email Address" required="">
                           </div>
                        </div>

                        <div class="col-sm-6">
                           <div class="form-group">
                              <input class="form-control" name="username" type="text" placeholder="Name" required="">
                           </div>
                        </div>

                         <div class="col-sm-12">
                           <div class="form-group">
                              <select  class="form-control" name="comment_status">
                              <option selected="selected">Select Business Category</option>
                              <option value="Excellent">Excellent</option>
                              <option value="Very Good">Very Good</option>
                              <option value="Good">Good</option>
                              <option value="Bad">Bad</option>
                              <option value="Very Bad">Very Bad</option>
                          </select>
                           </div>
                        </div>
                        
                        <div class="col-12">
                           <div class="form-group">
                              <textarea class="form-control w-100" name="comment" cols="30" rows="4"
                                 placeholder="Write Comment" required=""></textarea>
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <button type="submit" name="posted" class="button button-contactForm">Post Comment</button>
                     </div>
                  </form>
                   <?php }} ?>
               </div>

                  <?php
                }?>   
            </div>
            <div class="col-lg-4">
               <div class="blog_right_sidebar">
                  <aside class="single_sidebar_widget search_widget">
                     <form action="#">
                        <div class="form-group">
                           <div class="input-group mb-3">
                              <input type="text" class="form-control" placeholder='Search Keyword'
                                 onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search Keyword'">
                              <div class="input-group-append">
                                 <button class="btn" type="button"><i class="ti-search"></i></button>
                              </div>
                           </div>
                        </div>
                        <button class="button rounded-0 primary-bg text-white w-100 btn_4" type="submit">Search</button>
                     </form>
                  </aside>
                  <aside class="single_sidebar_widget post_category_widget">
                    <h4 class="widget_title">Our Business Category</h4>
                    <ul class="list cat-list">
                        <li>
                            <a href="#" class="d-flex">
                                <p>Resaurant Food</p>                         
                            </a>
                        </li>
                        <li>
                            <a href="#" class="d-flex">
                                <p>Hotels</p>
                            </a>
                        </li>
                      
                    </ul>
                </aside>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!--================Blog Area end =================-->

   <!-- footer part start-->
  <?php include('footer.php');?>
   <!-- footer part end-->

   <!-- jquery plugins here-->
   <!-- jquery -->
   <script src="js/jquery-1.12.1.min.js"></script>
   <!-- popper js -->
   <script src="js/popper.min.js"></script>
   <!-- bootstrap js -->
   <script src="js/bootstrap.min.js"></script>
   <!-- easing js -->
   <script src="js/jquery.magnific-popup.js"></script>
   <!-- swiper js -->
   <script src="js/swiper.min.js"></script>
   <!-- swiper js -->
   <script src="js/masonry.pkgd.js"></script>
   <!-- particles js -->
   <script src="js/owl.carousel.min.js"></script>
   <!-- swiper js -->
   <script src="js/slick.min.js"></script>
   <script src="js/gijgo.min.js"></script>
   <script src="js/jquery.nice-select.min.js"></script>
   <!-- custom js -->
   <script src="js/custom.js"></script>
</body>

</html>