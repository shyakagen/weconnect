<?php
session_start();
error_reporting();
// DB CREDENTIALS
define('DB_HOST','localhost');
define('DB_USER','root');
define('DB_PASS','');
define('DB_NAME','we_connect_db');

//ESTABLISH DATABASE CONNECTION

try{
    $conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME,DB_USER, DB_PASS,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
}
catch(PDOException $e){
    exit("Error:". $e->getMessage());
}
//CHECK I THE FORM HAS BEEN SUBMITTED

if(isset($_POST['posted'])){

  //VARIABLE
  $business_id      = htmlentities($_POST['business_id']);
  $email_comment    = htmlentities($_POST['email_comment']);
  $username         = htmlentities($_POST['username']);
  $comment          = htmlentities($_POST['comment']);
  $time_comment     = date("H:i:s");

  //SQL QUERY FOR SAVING

  $sql              =  "INSERT INTO comment(email_comment,username,comment,time_comment,business_id)VALUES(:email_comment,:username,:comment,:time_comment,:business_id)";
  $query            = $conn->prepare($sql);
  $query->bindParam(':email_comment',$email_comment,PDO::PARAM_STR);
  $query->bindParam(':username',$username,PDO::PARAM_STR);
  $query->bindParam(':comment',$comment,PDO::PARAM_STR);
  $query->bindParam(':time_comment',$time_comment,PDO::PARAM_STR);
  $query->bindParam(':business_id',$business_id,PDO::PARAM_STR);
  $query->execute();
  if($query){
    echo "<script>alert('Successfully Posted'); window.location = 'index.php';</script>";

  }else{
     echo "<script>alert('Failed To Posted'); window.location = 'comment.php';</script>";
  }
}

?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Weconnect</title>
    <link rel="icon" href="logo.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/gijgo.min.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/all.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <?php include('header_nav.php');?>
   <!--================Blog Area =================-->
   <section class="blog_area single-post-area section_padding">

      <div class="container">
         <div class="row">
            <div class="col-lg-8 posts-list">
              <?php
                   //connection DB 
                   include('config.php');
                   $business_id = $_GET['business_id'];
                   $select = $conn->prepare("SELECT * FROM business WHERE business_id=:business_id");
                   $select->bindParam(':business_id',$business_id,PDO::PARAM_STR);
                   $select->setFetchMode(PDO::FETCH_ASSOC);
                   $select->execute();
                   while($data=$select->fetch()){
                    ?>
               <div class="single-post">
                  <div class="feature-img">
                      <img src="uploads/<?php echo $data['business_logo'];?>">
                  </div>
               </div>
             </br>


               <div class="">

                <h5 ><a class="genric-btn primary circle arrow" href="view_comment.php?business_id=<?php echo $data['business_id'];?>"><font color=""><?php echo $data['business_logo'];?></font></a></h5>

                 <!--  <h4 ><a class="genric-btn primary circle arrow" href="view_comment.php?business_id=<?php echo $data['business_id'];?>"><font color="">View Previous Comments</font></a></h4> -->
               </div>
               <div class="comment-form">
                  <h5>Leave Your  Comment</h5>

                  <form method = "POST" class="form-contact comment_form" action="<?php echo $_SERVER['PHP_SELF'];?>" >
                     <?php 
                      // DB CREDENTIALS
                     include('config.php');
                 
                  $business_id = $_GET['business_id'];
                  $sql = "SELECT * FROM business WHERE business_id=:business_id";                              
                  $query = $conn->prepare($sql);
                  $query->bindParam(':business_id',$business_id,PDO::PARAM_STR);
                  $query->execute();
                  $results=$query->fetchAll(PDO::FETCH_OBJ);
                  $cnt=1;
                  if($query->rowCount() > 0)
                  {
                  foreach($results as $result)
                  {   
                      ?>  
                     <div class="row">

                      <div class="col-sm-12">
                           <div class="form-group">
                              <input class="form-control" name="business_id" id="business_id" value="<?php echo htmlentities($result->business_id);?>" type="hidden" placeholder="business_id">
                           </div>
                        </div>

                        <div class="col-sm-6">
                           <div class="form-group">
                             <input class="form-control" name="email_comment" type="email" placeholder="Email Address" required="">
                           </div>
                        </div>

                        <div class="col-sm-6">
                           <div class="form-group">
                              <input class="form-control" name="username" type="text" placeholder="Name" required="">
                           </div>
                        </div>

                        <div class="col-12">
                           <div class="form-group">
                              <textarea class="form-control w-100" name="comment" cols="30" rows="4"
                                 placeholder="Write Comment" required=""></textarea>
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <button type="submit" name="posted" class="button button-contactForm">Post Comment</button>
                     </div>
                  </form>
                   <?php }} ?>
               </div>

                  <?php
                }?>   
            </div>
            <div class="col-lg-4">
               <div class="blog_right_sidebar">
                  <aside class="single_sidebar_widget search_widget">
                     <form action="#">
                        <div class="form-group">
                           <div class="input-group mb-3">
                              <input type="text" class="form-control" placeholder='Search Keyword'
                                 onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search Keyword'">
                              <div class="input-group-append">
                                 <button class="btn" type="button"><i class="ti-search"></i></button>
                              </div>
                           </div>
                        </div>
                        <button class="button rounded-0 primary-bg text-white w-100 btn_4" type="submit">Search</button>
                     </form>
                  </aside>
                  <aside class="single_sidebar_widget post_category_widget">
                    <h4 class="widget_title">Category</h4>
                    <ul class="list cat-list">
                        <li>
                            <a href="#" class="d-flex">
                                <p>Resaurant Food</p>                         
                            </a>
                        </li>
                        <li>
                            <a href="#" class="d-flex">
                                <p>Hotels</p>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="d-flex">
                                <p>Bar</p>
                            </a>
                        </li>  
                    </ul>
                </aside>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!--================Blog Area end =================-->

   <!-- footer part start-->
   <footer class="footer-area">
      <div class="container">
         <div class="row">
            <div class="col-xl-3 col-sm-6 col-md-4">
               <div class="single-footer-widget footer_1">
                  <h4>About Us</h4>
                  <p>Heaven fruitful doesn't over for these theheaven fruitful doe over days
                     appear creeping seasons sad behold beari ath of it fly signs bearing
                     be one blessed after.</p>
               </div>
            </div>
            <div class="col-xl-3 col-sm-6 col-md-4">
               <div class="single-footer-widget footer_2">
                  <h4>Important Link</h4>
                  <div class="contact_info">
                     <ul>
                        <li><a href="#">WHMCS-bridge</a></li>
                        <li><a href="#"> Search Domain</a></li>
                        <li><a href="#">My Account</a></li>
                        <li><a href="#">Shopping Cart</a></li>
                        <li><a href="#"> Our Shop</a></li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="col-xl-3 col-sm-6 col-md-4">
               <div class="single-footer-widget footer_2">
                  <h4>Contact us</h4>
                  <div class="contact_info">
                     <p><span> Address :</span>Hath of it fly signs bear be one blessed after </p>
                     <p><span> Phone :</span> +2 36 265 (8060)</p>
                     <p><span> Email : </span>info@colorlib.com </p>
                  </div>
               </div>
            </div>
            <div class="col-xl-3 col-sm-8 col-md-6">
               <div class="single-footer-widget footer_3">
                  <h4>Newsletter</h4>
                  <p>Heaven fruitful doesn't over lesser in days. Appear creeping seas</p>
                  <form action="#">
                     <div class="form-group">
                        <div class="input-group mb-3">
                           <input type="text" class="form-control" placeholder='Email Address'
                              onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address'">
                           <div class="input-group-append">
                              <button class="btn" type="button"><i class="fas fa-paper-plane"></i></button>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
         <div class="copyright_part_text">
            <div class="row">
               <div class="col-lg-8">
                  <p class="footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="ti-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
               </div>
               <div class="col-lg-4">
                  <div class="copyright_social_icon text-right">
                     <a href="#"><i class="fab fa-facebook-f"></i></a>
                     <a href="#"><i class="fab fa-twitter"></i></a>
                     <a href="#"><i class="ti-dribbble"></i></a>
                     <a href="#"><i class="fab fa-behance"></i></a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </footer>
   <!-- footer part end-->

   <!-- jquery plugins here-->
   <!-- jquery -->
   <script src="js/jquery-1.12.1.min.js"></script>
   <!-- popper js -->
   <script src="js/popper.min.js"></script>
   <!-- bootstrap js -->
   <script src="js/bootstrap.min.js"></script>
   <!-- easing js -->
   <script src="js/jquery.magnific-popup.js"></script>
   <!-- swiper js -->
   <script src="js/swiper.min.js"></script>
   <!-- swiper js -->
   <script src="js/masonry.pkgd.js"></script>
   <!-- particles js -->
   <script src="js/owl.carousel.min.js"></script>
   <!-- swiper js -->
   <script src="js/slick.min.js"></script>
   <script src="js/gijgo.min.js"></script>
   <script src="js/jquery.nice-select.min.js"></script>
   <!-- custom js -->
   <script src="js/custom.js"></script>
</body>

</html>