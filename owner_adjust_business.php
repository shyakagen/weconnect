<?php
session_start();
error_reporting();
?>
<!doctype html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Weconnect</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="icon" href="logo.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/gijgo.min.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/all.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="css/style.css">

    <link rel="stylesheet" type="text/css" href="css/styless.css" media="all" />
    <link rel="stylesheet" type="text/css" href="css/demo.css" media="all" />
    <!-- jQuery -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>

    <!-- FlexSlider -->
    <script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" charset="utf-8">
        var $ = jQuery.noConflict();
        $(window).load(function() {
            $('.flexslider').flexslider({
              animation: "fade"
          });

            $(function() {
                $('.show_menu').click(function(){
                    $('.menu').fadeIn();
                    $('.show_menu').fadeOut();
                    $('.hide_menu').fadeIn();
                });
                $('.hide_menu').click(function(){
                    $('.menu').fadeOut();
                    $('.show_menu').fadeIn();
                    $('.hide_menu').fadeOut();
                });
            });
        });
    </script>
</head>
<body>
 <?php include('owner_header.php');?>
<!--================Blog Area =================-->
    <!-- about part start-->
    <section class="about_part about_bg">
            <?php
            session_start();
           include('config.php');
          $credidential_id = $_SESSION['credidential_id'];
          $select = $conn->prepare("SELECT  * FROM business b,credidential c WHERE b.credidential_id =c.credidential_id AND b.credidential_id='".$_SESSION['credidential_id']."'");

          
           // $select = $conn->prepare("SELECT  * FROM business b,credidential c WHERE b.credidential_id =c.credidential_id AND b.credidential_id='".$_SESSION['credidential_id']."'");
          $select->setFetchMode(PDO::FETCH_ASSOC);
          $select->execute();
            while($data=$select->fetch()){
            ?>
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-4 col-lg-5 offset-lg-1">
                    <div class="about_img">
                       <img style="border-radius: 50%;" src="uploads/<?php echo $data['business_logo'];?>">
                    </div>
                </div>
                <div class="col-sm-8 col-lg-4">
                    <div class="about_text">
                        <h5><?php echo $data['business_name'];?></h5>
                        <h2><?php echo $data['business_location'];?></h2>
                          <h2><?php echo $data['business_category'];?></h2>
                        <p><?php echo $data['business_description'];?> </p><br/>
                            <a href="owner_edit_business.php?business_id=<?php echo $data['business_id'];?>" class="button rounded-0 primary-bg text-white w-100 btn_4">
                                <font style="color:white"><i>Update Business Details</i></font> 
                            </a>
                    </div>
                </div>
            </div>
        </div>
           <?php
        }?>   
    </section>
    <!-- about part end-->

<!--================Blog Area =================-->
<!-- footer part start-->
<?php include('footer.php');?>
<!-- footer part end-->
<!-- jquery plugins here-->
<!-- jquery -->
<script src="js/jquery-1.12.1.min.js"></script>
<!-- popper js -->
<script src="js/popper.min.js"></script>
<!-- bootstrap js -->
<script src="js/bootstrap.min.js"></script>
<!-- easing js -->
<script src="js/jquery.magnific-popup.js"></script>
<!-- swiper js -->
<script src="js/swiper.min.js"></script>
<!-- swiper js -->
<script src="js/masonry.pkgd.js"></script>
<!-- particles js -->
<script src="js/owl.carousel.min.js"></script>
<!-- swiper js -->
<script src="js/slick.min.js"></script>
<script src="js/gijgo.min.js"></script>
<script src="js/jquery.nice-select.min.js"></script>
<!-- custom js -->
<script src="js/custom.js"></script>
</body>

</html>