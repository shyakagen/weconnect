  <!--::header part start::-->
    <header class="main_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="admin_dash.php"> <img src="logo.png" alt="logo"> </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse main-menu-item justify-content-end"
                    id="navbarSupportedContent">


                    <ul class="navbar-nav">
                        <!-- Authentication Links -->

                        <li class="nav-item">
                            <a class="nav-link" href=""></a>
                        </li>          

                        <div class="collapse navbar-collapse main-menu-item justify-content-end"
                        id="navbarSupportedContent">

                        <ul class="navbar-nav">

                            <li class="nav-item">
                            <a class="nav-link" href="admin_dash.php">Home</a>
                        </li>

                            <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="blog.html" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Business 
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="admin_add_business_owner.php">Register Owner</a>
                                        <a class="dropdown-item" href="admin_view_owner.php">View Owner</a>
                                          <a class="dropdown-item" href="admin_view_business.php">View Business</a>
                                          <!--  <a class="dropdown-item" href="report_restaurant.php">View Restaurant</a> -->
                                          
                                          <!--<a class="dropdown-item" href="approve_appointment.php">Approving Appointment</a> -->
                                    </div>
                                </li>

                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="blog.html" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Report 
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    
                                            <a class="dropdown-item" href="report_all.php">General Report Hotels and Restaurants</a>
                                            <a class="dropdown-item" href="business1.php">Report Business Performance</a>
                                    </div>
                                </li>

                                 <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="blog.html" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      RDB ADMIN
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="logout.php">Logout</a>
                                    </div>
                                </li>
                    </ul>
                </div>
               
        </ul>
    </div>
</nav>
</div>
</div>
</div>
</header>