
<?php
session_start();
error_reporting();
if(!isset($_SESSION['username'])){ 
    header ("Location: login.php"); 
    exit;
}
//DATABASE CONNECTION

include('config.php');

//check if the form has been sumbited

if(isset($_POST['upload'])){


    //SET VARIABLE

    $business_id            =$_POST['business_id'];
    $business_name          =$_POST['business_name'];
    $business_category      =$_POST['business_category'];
    $business_description   =$_POST['business_description'];
    $business_location      =$_POST['business_location'];
    $business_logo          =$_POST['business_logo'];



        //LET MOVE OUR IMAGES

        move_uploaded_file($_FILES["business_logo"]["tmp_name"],"uploads/" . $_FILES["business_logo"]["name"]);         
        $business_logo=$_FILES["business_logo"]["name"];

        //LET DO OUR QUERY FOR UPDATING OUR DATA 

        $sql  = "UPDATE business SET business_name=:business_name,business_category=:business_category,business_description=:business_description,business_location=:business_location,business_logo=:business_logo WHERE business_id=:business_id";
        $query = $conn->prepare($sql);
        $query->bindParam(':business_name',$business_name,PDO::PARAM_STR);
        $query->bindParam(':business_category',$business_category,PDO::PARAM_STR);
        $query->bindParam(':business_description',$business_description,PDO::PARAM_STR);
        $query->bindParam(':business_location',$business_location,PDO::PARAM_STR);
        $query->bindParam(':business_logo',$business_logo,PDO::PARAM_STR);
        $query->bindParam(':business_id',$business_id,PDO::PARAM_STR);
        $query->execute();
        if($query){
               $msg= "Business Successful Created";

        }else{
            $error= "Failed to Update";

        }
    
}
?>
<!doctype html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Weconnect</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="icon" href="logo.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/gijgo.min.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/all.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="css/style.css">

    <link rel="stylesheet" type="text/css" href="css/styless.css" media="all" />
    <link rel="stylesheet" type="text/css" href="css/demo.css" media="all" />
    <!-- jQuery -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>

    <!-- FlexSlider -->
    <script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" charset="utf-8">
        var $ = jQuery.noConflict();
        $(window).load(function() {
            $('.flexslider').flexslider({
              animation: "fade"
          });

            $(function() {
                $('.show_menu').click(function(){
                    $('.menu').fadeIn();
                    $('.show_menu').fadeOut();
                    $('.hide_menu').fadeIn();
                });
                $('.hide_menu').click(function(){
                    $('.menu').fadeOut();
                    $('.show_menu').fadeIn();
                    $('.hide_menu').fadeOut();
                });
            });
        });
    </script>
</head>
<body>
 <?php include('owner_header.php');?>
<!--================Blog Area =================-->
<section class="blog_area section_padding">
    <div class="container">
        <div class="row">

            <div class="col-lg-7">
                <div class="blog_right_sidebar">
                    <aside class="single_sidebar_widget search_widget">
                          <p align="center"><b>Business Updating Form</b></p>

                     <form method="POST" action="<?php echo $_SERVER['PHP_SELF'];?>"  enctype="multipart/form-data">
                        <?php 
                    // DB CREDENTIALS
                        include('config.php');
                      $business_id = $_GET['business_id'];
                      $sql = "SELECT * FROM business WHERE business_id=:business_id";                              
                      $query = $conn->prepare($sql);
                      $query->bindParam(':business_id',$business_id,PDO::PARAM_STR);
                      $query->execute();
                      $results=$query->fetchAll(PDO::FETCH_OBJ);
                      $cnt=1;
                      if($query->rowCount() > 0)
                      {
                          foreach($results as $result)
                          {   
                              ?>  

                              <div class="form-group row">
                                        <label for="business_id" class="col-md-4 col-form-label text-md-right"></label>
                                        <div class="col-md-8">
                                            <input id="business_id" type="hidden" class="form-control " name="business_id" value="<?php echo htmlentities($result->business_id);?>" required autocomplete="business_id">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="tin_number" class="col-md-4 col-form-label text-md-right">Your Tin number Is</label>
                                        <div class="col-md-8">
                                            <input id="tin_number" type="text"  class="form-control " name="tin_number" value="<?php echo htmlentities($result->tin_number);?>" readonly required autocomplete="off">
                                            
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label for="business_name" class="col-md-4 col-form-label text-md-right">Business name</label>

                                        <div class="col-md-8">
                                            <input id="business_name" type="text" class="form-control " name="business_name" value="<?php echo htmlentities($result->business_name);?>" required autocomplete="business_name">
                                        </div>
                                    </div>

                                     <div class="form-group row">
                                        <label for="last_name" class="col-md-4 col-form-label text-md-right">Business Category</label>

                                        <div class="col-md-8">
                                            <select  class="form-control " name="business_category">
                                                <option value="disabled">Selected Business Category</option>
                                                <option value="Hotels">Hotels</option>
                                                <option value="Restaurant">Restaurant</option>
                                                <option value="Bar">Bar</option>

                                            </select>
                                        </div>
                                    </div>

                                     <div class="form-group row">
                                        <label for="email_address" class="col-md-4 col-form-label text-md-right">Business Description</label>
                                        <div class="col-md-8">
                                            <textarea class="form-control" name="business_description">
                                                <?php echo htmlentities($result->business_description);?>
                                            </textarea>
                                        </div>
                                    </div>

                                     <div class="form-group row">
                                    <label for="telephone_number" class="col-md-4 col-form-label text-md-right">Business location</label>

                                    <div class="col-md-8">
                                        <input id="business_location" type="text" class="form-control " name="business_location" value="<?php echo htmlentities($result->business_location);?>" required autocomplete="business_logo">
                                    </div>
                                </div> 

                                  <div class="form-group row">
                                        <label for="email" class="col-md-4 col-form-label text-md-right">BUSINESS LOGO</label>
                                        <div class="col-md-6">
                                           <input type="hidden" name="size" value="1000000" class="form-control" />
                                           <input id="" type="file" name="business_logo" class="form-control" />
                                       </div>
                                   </div>

                            <input class="button rounded-0 primary-bg text-white w-100 btn_4" type="hidden" value="Record" />
                            <input class="button rounded-0 primary-bg text-white w-100 btn_4" type="submit" name="upload" value="Business Information" />
                        </form>
                     <?php }} ?>
                </aside>

            </div>
        </div>

        <div class="col-lg-4">
            <div class="blog_right_sidebar">
                <aside class="single_sidebar_widget search_widget">
                    <form>
                        <?php
                        include('config.php');
          $credidential_id = $_SESSION['credidential_id'];
          $select = $conn->prepare("SELECT  * FROM business b,credidential c WHERE b.credidential_id =c.credidential_id AND b.credidential_id='".$_SESSION['credidential_id']."'");
          $select->setFetchMode(PDO::FETCH_ASSOC);
          $select->execute();
            while($data=$select->fetch()){
            ?>
                            <div class="single_blog_item">
                                <div class="single_blog_img">
                                   <img style="border-radius-left: 50%;" src="uploads/<?php echo $data['business_logo'];?>">
                                </div>
                                <div class="single_blog_text text-center">

                                    <p align="left"><b>Business name</b> :  <?php echo $data['business_name'];?></p>
                                    <p align="left"><b>Your Tin number</b> :  <?php echo $data['tin_number'];?></p>
                                    <p align="left"><b>Business Location</b> : <?php echo $data['business_location'];?></p>
                                    <p align="left"><b>Business Description</b>:  <?php echo $data['business_description'];?> </p>
                                </div>
                            </div>
                                <font class="button rounded-0 primary-bg text-white w-100 btn_4" style="color:white"><i>Business Information</i></font> 
                          
                        </form>
                        <?php
        }?>   
                </aside>
            </div>
        </div>

    </div>
</div>
</section>
<!--================Blog Area =================-->
<!-- footer part start-->
<?php include('footer.php');?>
<!-- footer part end-->
<!-- jquery plugins here-->
<!-- jquery -->
<script src="js/jquery-1.12.1.min.js"></script>
<!-- popper js -->
<script src="js/popper.min.js"></script>
<!-- bootstrap js -->
<script src="js/bootstrap.min.js"></script>
<!-- easing js -->
<script src="js/jquery.magnific-popup.js"></script>
<!-- swiper js -->
<script src="js/swiper.min.js"></script>
<!-- swiper js -->
<script src="js/masonry.pkgd.js"></script>
<!-- particles js -->
<script src="js/owl.carousel.min.js"></script>
<!-- swiper js -->
<script src="js/slick.min.js"></script>
<script src="js/gijgo.min.js"></script>
<script src="js/jquery.nice-select.min.js"></script>
<!-- custom js -->
<script src="js/custom.js"></script>
</body>

</html>