  <!--::header part start::-->
    <header class="main_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="index.php"> <img src="logo.png" alt="logo"> </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse main-menu-item justify-content-end"
                            id="navbarSupportedContent">

                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link" href="index.php">Home</a>
                                </li>

                                  <li class="nav-item">
                                    <a class="nav-link" href="">Contact</a>
                                </li>

                            </ul>
                        </div>

                        <div class="menu_btn">
                            <a href="login.php" class="single_page_btn d-none d-sm-block" style="margin: 20px; ">Login</a>
                        </div>

                        <div class="menu_btn">
                            <a href="registration.php" class="single_page_btn d-none d-sm-block">Register</a>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- Header part end-->