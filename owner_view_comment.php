<?php
session_start();
error_reporting();
if(!isset($_SESSION['username'])){ 
header ("Location:login.php"); 
exit;
}
?>
<!doctype html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Weconnect</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="icon" href="logo.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/gijgo.min.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/all.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="css/style.css">

    <link rel="stylesheet" type="text/css" href="css/styless.css" media="all" />
    <link rel="stylesheet" type="text/css" href="css/demo.css" media="all" />
    <!-- jQuery -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>

    <!-- FlexSlider -->
    <script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" charset="utf-8">
        var $ = jQuery.noConflict();
        $(window).load(function() {
            $('.flexslider').flexslider({
              animation: "fade"
          });

            $(function() {
                $('.show_menu').click(function(){
                    $('.menu').fadeIn();
                    $('.show_menu').fadeOut();
                    $('.hide_menu').fadeIn();
                });
                $('.hide_menu').click(function(){
                    $('.menu').fadeOut();
                    $('.show_menu').fadeIn();
                    $('.hide_menu').fadeOut();
                });
            });
        });
    </script>
</head>
<body>
 <?php include('owner_header.php');?>

<!--================Blog Area =================-->
<section class="blog_area section_padding">
    <div class="container">
        <div class="row">

            <div class="col-lg-7">
                <div class="blog_right_sidebar">
                    <aside class="single_sidebar_widget search_widget">
                   <div class="comments-area">
                  <h4><a href="">All Business Related Comments</a></h4>

                  <div class="comment-list">
                  <?php 
                  include('config.php');
                  $business_id = $_GET['business_id'];
                  $sql = "SELECT * FROM comment,business WHERE business.business_id = comment.business_id  AND business.business_id=:business_id ORDER BY comment_id DESC";                              
                  $query = $conn->prepare($sql);
                  $query->bindParam(':business_id',$business_id,PDO::PARAM_STR);
                  $query->execute();
                  $results=$query->fetchAll(PDO::FETCH_OBJ);
                  $cnt=1;
                  if($query->rowCount() > 0)
                  {
                  foreach($results as $result)
                  {   
                      ?>
                     <div class="single-comment justify-content-between d-flex">
                        <div class="user justify-content-between d-flex">
                           <div class="thumb">
                          
                              <img src="avatar_male.png" alt=""><br/><br/><br/><br/>
                           </div>
                           <div class="desc">

                            <h5>
                                       <a href="#"><b><?php echo htmlentities($result->username);?></b></a><br/><br/>
                                    </h5>
                              <p class="comment">
                                <?php echo htmlentities($result->comment);?>
                              </p>
                              <div class="d-flex justify-content-between">
                                 <div class="d-flex align-items-center">
                                    
                                    <p class="date"><?php echo htmlentities($result->date);?></p><br/><br/>
                                 </div>
                              </div>
                           </div>
                        </div>

                     </div>
                      <?php }} ?>
                  </div>
            
               </div>
               
                </aside>

            </div>
        </div>

        <div class="col-lg-4">
            <div class="blog_right_sidebar">
                <aside class="single_sidebar_widget search_widget">
                    <form>
                      <?php 
                  include('config.php');
                  $business_id = $_GET['business_id'];
                  $sql = "SELECT  * FROM business b,credidential c WHERE b.credidential_id =c.credidential_id AND b.credidential_id='".$_SESSION['credidential_id']."'";  

                  //QUERY WITH SESSION OF USER_ID

                   // $select = $conn->prepare("SELECT  * FROM business b,credidential c WHERE b.credidential_id =c.credidential_id AND b.credidential_id='".$_SESSION['credidential_id']."'");

                  $query = $conn->prepare($sql);
                  $query->bindParam(':business_id',$business_id,PDO::PARAM_STR);
                  $query->execute();
                  $results=$query->fetchAll(PDO::FETCH_OBJ);
                  $cnt=1;
                  if($query->rowCount() > 0)
                  {
                  foreach($results as $result)
                  {   
                      ?>
                            <div class="single_blog_item">
                                <div class="single_blog_img">
                                  <img src="uploads/<?php echo htmlentities($result->business_logo);?>">
                                </div>
                                <div class="single_blog_text text-center">
                                    <p><?php echo htmlentities($result->business_description);?></p>
                                </div>
                            </div>
                            <a href="owner_view_comment.php?business_id=<?php echo htmlentities($result->business_id);?>" class="button rounded-0 primary-bg text-white w-100 btn_4">
                                View Comment
                            </a>
                        </form>
                      <?php }} ?>
                </aside>
            </div>
        </div>

    </div>
</div>
</section>
<!--================Blog Area =================-->
<!-- footer part start-->
<?php include('footer.php');?>
<!-- footer part end-->
<!-- jquery plugins here-->
<!-- jquery -->
<script src="js/jquery-1.12.1.min.js"></script>
<!-- popper js -->
<script src="js/popper.min.js"></script>
<!-- bootstrap js -->
<script src="js/bootstrap.min.js"></script>
<!-- easing js -->
<script src="js/jquery.magnific-popup.js"></script>
<!-- swiper js -->
<script src="js/swiper.min.js"></script>
<!-- swiper js -->
<script src="js/masonry.pkgd.js"></script>
<!-- particles js -->
<script src="js/owl.carousel.min.js"></script>
<!-- swiper js -->
<script src="js/slick.min.js"></script>
<script src="js/gijgo.min.js"></script>
<script src="js/jquery.nice-select.min.js"></script>
<!-- custom js -->
<script src="js/custom.js"></script>
</body>

</html>