
<?php
session_start();
error_reporting();
if(!isset($_SESSION['username'])){ 
    header ("Location: login.php"); 
    exit;
}
//DATABASE CONNECTION

include('config.php');

//check if the form has been sumbited

if(isset($_POST['upload'])){


    //SET VARIABLE

    $credidential_id        =$_POST['credidential_id'];
    $date_appointment       =$_POST['date_appointment'];
    $status                 = "Request";


        //LET DO OUR QUERY FOR UPDATING OUR DATA 


    $sql  = "INSERT INTO appointment(date_appointment,status,credidential_id)VALUES(:date_appointment,:status,:credidential_id)";
    $query = $conn->prepare($sql);
    $query->bindParam(':date_appointment',$date_appointment,PDO::PARAM_STR);
    $query->bindParam(':status',$status,PDO::PARAM_STR);
    $query->bindParam(':credidential_id',$credidential_id,PDO::PARAM_STR);
    $query->execute();
    $msg= "Business Successful Created";
    }
?>
<!doctype html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Weconnect</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="icon" href="logo.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/gijgo.min.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/all.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="css/style.css">

    <link rel="stylesheet" type="text/css" href="css/styless.css" media="all" />
    <link rel="stylesheet" type="text/css" href="css/demo.css" media="all" />
    <!-- jQuery -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>

    <!-- FlexSlider -->
    <script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" charset="utf-8">
        var $ = jQuery.noConflict();
        $(window).load(function() {
            $('.flexslider').flexslider({
              animation: "fade"
          });

            $(function() {
                $('.show_menu').click(function(){
                    $('.menu').fadeIn();
                    $('.show_menu').fadeOut();
                    $('.hide_menu').fadeIn();
                });
                $('.hide_menu').click(function(){
                    $('.menu').fadeOut();
                    $('.show_menu').fadeIn();
                    $('.hide_menu').fadeOut();
                });
            });
        });
    </script>
</head>
<body>
   <?php include('owner_header.php');?>
   <!-- Header part end-->
   <!--================Blog Area =================-->
   <section class="blog_area section_padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="blog_right_sidebar">
                    <aside class="single_sidebar_widget search_widget">
                        <?php if($msg){?>
                            <div class="alert alert-success left-icon-alert" role="alert">
                               <strong>Well done!</strong><?php echo htmlentities($msg); ?>
                               </div><?php } 
                               else if($error){?>
                                <div class="alert alert-danger left-icon-alert" role="alert">
                                    <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                                </div>
                            <?php } ?>
                            <p align="center"><b>Business Appointment Form</b></p>

                            <form method="POST" action="<?php echo $_SERVER['PHP_SELF'];?>"  enctype="multipart/form-data">
                                <?php 
                                      // DB CREDENTIALS
                                define('DB_HOST','localhost');
                                define('DB_USER','phpmyadmin');
                                define('DB_PASS','kashya@12345');
                                define('DB_NAME','we_connect_db');

                             //ESTABLISH DATABASE CONNECTION

                                try{
                                  $conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME,DB_USER, DB_PASS,array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
                              }
                              catch(PDOException $e){
                                  exit("Error:". $e->getMessage());
                              }
                              $credidential_id = $_GET['credidential_id'];
                              $sql = "SELECT * FROM credidential  WHERE credidential_id='".$_SESSION['credidential_id']."'";                           
                              $query = $conn->prepare($sql);
                              $query->bindParam(':credidential_id',$credidential_id,PDO::PARAM_STR);
                              $query->execute();
                              $results=$query->fetchAll(PDO::FETCH_OBJ);
                              $cnt=1;
                              if($query->rowCount() > 0)
                              {
                                  foreach($results as $result)
                                  {   
                                      ?>  

                                      <div class="form-group row">
                                        <label for="credidential_id" class="col-md-4 col-form-label text-md-right"></label>
                                        <div class="col-md-8">
                                            <input id="person_id" type="hidden" class="form-control " name="credidential_id" value="<?php echo htmlentities($result->credidential_id);?>" required autocomplete="credidential_id">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="tin_number" class="col-md-4 col-form-label text-md-right">Request Date</label>
                                        <div class="col-md-8">
                                            <input id="date_appointment" type="date"  class="form-control " name="date_appointment" required >
                                            
                                        </div>
                                    </div>

                                <input class="button rounded-0 primary-bg text-white w-100 btn_4" type="hidden" value="Record" />
                                <input class="button rounded-0 primary-bg text-white w-100 btn_4" type="submit" name="upload" value="Request Business Appointment" />

                            </form>
                        <?php }} ?>
                        
                    </aside>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="blog_right_sidebar">
                    
                 <aside class="single_sidebar_widget post_category_widget">
                    <h4 class="widget_title">Category</h4>
                    <ul class="list cat-list">
                        <li>
                            <a href="#" class="d-flex">
                                <p>Resaurant Food</p>                         
                            </a>
                        </li>
                        <li>
                            <a href="#" class="d-flex">
                                <p>Hotels</p>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="d-flex">
                                <p>Bar</p>
                            </a>
                        </li>  
                    </ul>
                </aside>
            </div>
        </div>
    </div>
</div>
</section>
<!--================Blog Area =================-->
<!-- footer part start-->
<?php include('footer.php');?>
<!-- footer part end-->
<!-- jquery plugins here-->
<!-- jquery -->
<script src="js/jquery-1.12.1.min.js"></script>
<!-- popper js -->
<script src="js/popper.min.js"></script>
<!-- bootstrap js -->
<script src="js/bootstrap.min.js"></script>
<!-- easing js -->
<script src="js/jquery.magnific-popup.js"></script>
<!-- swiper js -->
<script src="js/swiper.min.js"></script>
<!-- swiper js -->
<script src="js/masonry.pkgd.js"></script>
<!-- particles js -->
<script src="js/owl.carousel.min.js"></script>
<!-- swiper js -->
<script src="js/slick.min.js"></script>
<script src="js/gijgo.min.js"></script>
<script src="js/jquery.nice-select.min.js"></script>
<!-- custom js -->
<script src="js/custom.js"></script>
</body>

</html>
