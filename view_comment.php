<?php
session_start();
error_reporting();
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Weconnect</title>
    <link rel="icon" href="logo.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/gijgo.min.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/all.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <?php include('header_nav.php');?>
   <!--================Blog Area =================-->
   <section class="blog_area single-post-area section_padding">

      <div class="container">
         <div class="row">
            <div class="col-lg-8 posts-list">
               <?php 
                  include('config.php');
                  $business_id = $_GET['business_id'];
                  $sql = "SELECT * FROM business WHERE business.business_id=:business_id";                              
                  $query = $conn->prepare($sql);
                  $query->bindParam(':business_id',$business_id,PDO::PARAM_STR);
                  $query->execute();
                  $results=$query->fetchAll(PDO::FETCH_OBJ);
                  $cnt=1;
                  if($query->rowCount() > 0)
                  {
                  foreach($results as $result)
                  {   
                      ?>
               <div class="single-post">
                  <div class="feature-img">
                   
                     <img src="uploads/<?php echo htmlentities($result->business_logo);?>">
                    
                  </div>
                
               </div>
                <?php }}?>
               <div class="comments-area">
                  <h4>All Comments</h4>
                     <?php 
                  include('config.php');
                  $business_id = $_GET['business_id'];
                  $sql = "SELECT * FROM comment,business WHERE business.business_id = comment.business_id AND business.business_id=:business_id ORDER BY comment_id DESC";                              
                  $query = $conn->prepare($sql);
                  $query->bindParam(':business_id',$business_id,PDO::PARAM_STR);
                  $query->execute();
                  $results=$query->fetchAll(PDO::FETCH_OBJ);
                  $cnt=1;
                  if($query->rowCount() > 0)
                  {
                  foreach($results as $result)
                  {   
                      ?>
                  <div class="blog-author">
                  <div class="media align-items-center">
                     <img src="avatar_male.png" alt="Weconnect">
                     <div class="media-body">
                           <h4><b><?php echo htmlentities($result->username);?><b></h4>

                        <p><b><?php echo htmlentities($result->sub_categs);?></b>  :&nbsp; &nbsp; <i><?php echo htmlentities($result->comment_status);?></i></p>
                         <p><?php echo htmlentities($result->comment);?></p>
                        <p class="date"><?php echo htmlentities($result->date);?></p>
                     </div>
                  </div>
               </div>
             <?php }} ?>
               </div>
            </div>
            <div class="col-lg-16">
               <div class="blog_right_sidebar">
                  <aside class="single_sidebar_widget search_widget">
                     <form action="#">
                        <div class="form-group">
                           <div class="input-group mb-3">
                              <input type="text" class="form-control" placeholder='Search Keyword'
                                 onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search Keyword'">
                              <div class="input-group-append">
                                 <button class="btn" type="button"><i class="ti-search"></i></button>
                              </div>
                           </div>
                        </div>
                        <button class="button rounded-0 primary-bg text-white w-100 btn_4" type="submit">Search</button>
                     </form>
                  </aside>
                  <aside class="single_sidebar_widget post_category_widget">
                    <h4 class="widget_title">Our Business Category</h4>
                    <ul class="list cat-list">
                        <li>
                            <a href="#" class="d-flex">
                                <p>Resaurant</p>                         
                            </a>
                        </li>
                        <li>
                            <a href="#" class="d-flex">
                                <p>Hotels</p>
                            </a>
                        </li>
                      
                    </ul>
                </aside>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!--================Blog Area end =================-->

   <!-- footer part start-->
  <?php include('footer.php');?>
   <!-- footer part end-->

   <!-- jquery plugins here-->
   <!-- jquery -->
   <script src="js/jquery-1.12.1.min.js"></script>
   <!-- popper js -->
   <script src="js/popper.min.js"></script>
   <!-- bootstrap js -->
   <script src="js/bootstrap.min.js"></script>
   <!-- easing js -->
   <script src="js/jquery.magnific-popup.js"></script>
   <!-- swiper js -->
   <script src="js/swiper.min.js"></script>
   <!-- swiper js -->
   <script src="js/masonry.pkgd.js"></script>
   <!-- particles js -->
   <script src="js/owl.carousel.min.js"></script>
   <!-- swiper js -->
   <script src="js/slick.min.js"></script>
   <script src="js/gijgo.min.js"></script>
   <script src="js/jquery.nice-select.min.js"></script>
   <!-- custom js -->
   <script src="js/custom.js"></script>
</body>

</html>