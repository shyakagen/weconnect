<?php  
 $connect = mysqli_connect("localhost", "phpmyadmin", "kashya@12345", "we_connect_db");  
 $query ="SELECT * FROM business,credidential WHERE business.credidential_id=credidential.credidential_id AND  business_category='Restaurant'";  
 $result = mysqli_query($connect, $query);
 ?>  
 <!DOCTYPE html>  
 <html>  
      <head>  
           <title>Report</title>  
             <link rel="icon" href="logo.png">
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
           <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
           <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>  
           <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>            
           <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" /> 
      </head>  
      <body>  
           <br /><br />  
           <div class="container">  
               <legend><img src="logo.png" alt="logo"></legend>
                <br />  
                <div class="table-responsive">  
                     <table id="students_data" class="table table-striped table-bordered">  
                        <LEGEND><p align="center" class="">RESTAURANT BUSINESS REPORT<i class="icon-pencil"></i></p><br/>

                            <p align="right"><b><?php echo "".date("D d-M-Y")." " ?> <?php echo " ".date("h:i:s A")." " ?> </b></p>   
                          <thead>  
                               <tr> <td>Business Onwer Name</td>  
                                    <td>Tin number</td>  
                                    <td>Business Name</td>  
                                    <td>Business Location</td>
                                    <td>Status</td> 
                                   
                               </tr>  
                          </thead>  
                          <?php  
                          while($row = mysqli_fetch_array($result))  
                          {  
                               echo '  
                               <tr> <td>'.$row["username"].'</td>  
                                    <td>'.$row["tin_number"].'</td>  
                                    <td>'.$row["business_name"].'</td>  
                                    <td>'.$row["business_location"].'</td>  
                                    <td>'.$row["status"].'</td>
                               </tr>  
                               ';  
                          }  
                          ?>
                     </table>  
                      <h1 align="right" style="size: 20px;"><a class="btn btn-success btn-small hidden-print" href="javascript:window.print()">Print</a></h1>
                      <h2><a href="admin_dash.php"><b>Back To Home</b></a><h2>
                </div>  
           </div>  
         
      </body>  
 </html>  
 <script>  
 $(document).ready(function(){  
      $('#students_data').DataTable();  
 });  
 </script>  