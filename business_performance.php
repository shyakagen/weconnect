<?php
session_start();
error_reporting();
include('config.php');

//check if the form has submit

if(isset($_POST['submit'])){

    //RECEIVE THE VALUE FROM VARIABLE

    $identity_number    = htmlentities($_POST['identity_number']);
    $first_name         = htmlentities($_POST['first_name']);
    $last_name          = htmlentities($_POST['last_name']);
    $email_address      = htmlentities($_POST['email_address']);
    $telephone_number   = htmlentities($_POST['telephone_number']);
    $person_id          = htmlentities($_POST['person_id']);

    //SQL QUERY THE FOR INSERTING 

    $sql   = "UPDATE person SET identity_number=:identity_number,first_name=:first_name,last_name=:last_name,email_address=:email_address,telephone_number=:telephone_number WHERE person_id=:person_id";

    $query = $conn->prepare($sql);
    $query->bindParam(':identity_number',$identity_number,PDO::PARAM_STR);
    $query->bindParam(':first_name',$first_name,PDO::PARAM_STR);
    $query->bindParam(':last_name',$last_name,PDO::PARAM_STR);
    $query->bindParam(':email_address',$email_address,PDO::PARAM_STR);
    $query->bindParam(':telephone_number',$telephone_number,PDO::PARAM_STR);
    $query->execute();

 //check if the query has success

    if($query){
        $msg = "Data successful Saved";
    }else{
        $errors = "Failed to saved something goes wrong";
    }
}


?>
<!doctype html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Weconnect</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="icon" href="logo.png">

    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/gijgo.min.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/all.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="css/style.css">

    <link rel="stylesheet" type="text/css" href="css/styless.css" media="all" />
    <link rel="stylesheet" type="text/css" href="css/demo.css" media="all" />
    <!-- jQuery -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>

    <!-- FlexSlider -->
    <script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" charset="utf-8">
        var $ = jQuery.noConflict();
        $(window).load(function() {
            $('.flexslider').flexslider({
              animation: "fade"
          });

            $(function() {
                $('.show_menu').click(function(){
                    $('.menu').fadeIn();
                    $('.show_menu').fadeOut();
                    $('.hide_menu').fadeIn();
                });
                $('.hide_menu').click(function(){
                    $('.menu').fadeOut();
                    $('.show_menu').fadeIn();
                    $('.hide_menu').fadeOut();
                });
            });
        });
    </script>
</head>
<body>
    <!-- Header part end-->
   <div class="container">
            <div class="row">
                <div class="col">
                    <p class="text-white mt-5 mb-5"></b></p>
                </div>
            </div>
            <!-- row -->
            <div class="row tm-content-row">
                <div class="col-12 tm-block-col">
                    <div class="tm-bg-primary-dark tm-block tm-block-taller tm-block-scroll">
                        <table class="table">
                            <legend><img src="logo.png" alt="logo"></legend>
                            <thead>
                                <LEGEND><p align="left" class=""><b>BUSINESS PERFOMANCE REPORT</b>
                                </p>
                                <form action="business1.php" method="POST">
                                <div class="form-group pull-right">
                                    <div class="form-row">
                                        <div class="col-sm-12">
                                            <div class="text-left text-muted">
                                                <select class="form-control">
                                                    <option selected="selected">Item Category</option>
                                                    <option value="Menu">Menu</option>
                                                    <option value="Parking">Parking</option>
                                                    <option value="CustomerCare">CustomerCare</option>
                                                </select>
                                                <input type="submit" value="Search" class="btn-primary">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>
                                <tr>
                                    <th scope="col">No</th>
                                     <th scope="col">TIN NUMBER</th>
                                    <th scope="col">BUSINESS NAME</th>
                                    <th scope="col">TYPE</th>
                                    <th scope="col">LOCATION</th>
                                    <th scope="col">CATEGORIES</th>
                                    <th scope="col">COMMENT</th>
                                    <th scope="col">GRADE</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                                      <?php 
                                   // include('config.php');
                                    $sql = "SELECT b.tin_number, b.business_name, b.business_category, b.business_location, c.sub_categs, c.comment_status, COUNT(C.comment_status) AS grade FROM business b, comment c WHERE c.business_id=b.business_id GROUP BY c.sub_categs";
                                    $query = $conn->prepare($sql);
                                    $query->execute();
                                    $results=$query->fetchAll(PDO::FETCH_OBJ);
                                    $cnt=1;
                                    if($query->rowCount() > 0)
                                    {
                                    foreach($results as $result)
                                    {   
                                        ?>  
                                <tr>
                                    <th scope="row"><b><?php echo $cnt;?></b></th>

                                           
                                    <!-- </td>
                                        </div><?php echo  htmlentities($result->username);?>
                                    </td> -->
                                    <td><b><?php echo htmlentities($result->tin_number);?></b></td>
                                    <td><b><?php echo htmlentities($result->business_name);?></b></td>
                                    <td><b><?php echo htmlentities($result->business_category);?></b></td>
                                    <td><b><?php echo htmlentities($result->business_location);?></b></td>
                                    <td><b><?php echo htmlentities($result->sub_categs);?></b></td>
                                    <td><b><?php echo htmlentities($result->comment_status);?></b></td>
                                    <td><b><?php echo htmlentities($result->grade);?></b></td>
                                    
                                </tr>    
                                    <?php $cnt=$cnt+1;}} ?>                      
                            </tbody>

                        </table>
                        
                        <h2 align="right" style="size: 20px;"><a class="btn btn-success btn-small hidden-print" href="javascript:window.print()">Print</a></h2>
                    </div>

                </div>
            </div>
        </div>
        <!--================Blog Area =================-->
        <!-- footer part start-->
        <footer class="footer-area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-3 col-sm-6 col-md-4">
                        <div class="single-footer-widget footer_1">
                            <h4>About Us</h4>
                            <p>Heaven fruitful doesn't over for these theheaven fruitful doe over  days 
                                appear creeping seasons sad behold beari ath of it fly signs bearing 
                            be one blessed after.</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-sm-6 col-md-4">
                        <div class="single-footer-widget footer_2">
                            <h4>Important Link</h4>
                            <div class="contact_info">
                                <ul>
                                    <li><a href="#">WHMCS-bridge</a></li>
                                    <li><a href="#"> Search Domain</a></li>
                                    <li><a href="#">My Account</a></li>
                                    <li><a href="#">Shopping Cart</a></li>
                                    <li><a href="#"> Our Shop</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-sm-6 col-md-4">
                        <div class="single-footer-widget footer_2">
                            <h4>Contact us</h4>
                            <div class="contact_info">
                                <p><span> Address :</span>Hath of it fly signs bear be one blessed after </p>
                                <p><span> Phone :</span> +2 36 265 (8060)</p>
                                <p><span> Email : </span>info@colorlib.com </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-sm-8 col-md-6">
                        <div class="single-footer-widget footer_3">
                            <h4>Newsletter</h4>
                            <p>Heaven fruitful doesn't over lesser in days. Appear creeping seas</p>
                            <form action="#">
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" placeholder='Email Address'
                                        onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address'">
                                        <div class="input-group-append">
                                            <button class="btn" type="button"><i class="fas fa-paper-plane"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="copyright_part_text">
                    <div class="row">
                        <div class="col-lg-8">
                            <p class="footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                Copyright &copy;Shyaka Gen All rights reserved | This template is made with <i class="ti-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Esther UMUBYEYI</a>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                            </div>
                            <div class="col-lg-4">
                                <div class="copyright_social_icon text-right">
                                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#"><i class="ti-dribbble"></i></a>
                                    <a href="#"><i class="fab fa-behance"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- footer part end-->
        <!-- jquery plugins here-->
        <!-- jquery -->
        <script src="js/jquery-1.12.1.min.js"></script>
        <!-- popper js -->
        <script src="js/popper.min.js"></script>
        <!-- bootstrap js -->
        <script src="js/bootstrap.min.js"></script>
        <!-- easing js -->
        <script src="js/jquery.magnific-popup.js"></script>
        <!-- swiper js -->
        <script src="js/swiper.min.js"></script>
        <!-- swiper js -->
        <script src="js/masonry.pkgd.js"></script>
        <!-- particles js -->
        <script src="js/owl.carousel.min.js"></script>
        <!-- swiper js -->
        <script src="js/slick.min.js"></script>
        <script src="js/gijgo.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <!-- custom js -->
        <script src="js/custom.js"></script>
    </body>

    </html>
<!doctype html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Weconnect</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="icon" href="logo.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/gijgo.min.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/all.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="css/style.css">

    <link rel="stylesheet" type="text/css" href="css/styless.css" media="all" />
    <link rel="stylesheet" type="text/css" href="css/demo.css" media="all" />
    <!-- jQuery -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>

    <!-- FlexSlider -->
    <script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" charset="utf-8">
        var $ = jQuery.noConflict();
        $(window).load(function() {
            $('.flexslider').flexslider({
              animation: "fade"
          });

            $(function() {
                $('.show_menu').click(function(){
                    $('.menu').fadeIn();
                    $('.show_menu').fadeOut();
                    $('.hide_menu').fadeIn();
                });
                $('.hide_menu').click(function(){
                    $('.menu').fadeOut();
                    $('.show_menu').fadeIn();
                    $('.hide_menu').fadeOut();
                });
            });
        });
    </script>
</head>
<body>
    <?php include('admin_header.php');?>
    <!-- Header part end-->
   <div class="container">
            <div class="row">
                <div class="col">
                    <p class="text-white mt-5 mb-5"></b></p>
                </div>
            </div>
            <!-- row -->
            <div class="row tm-content-row">
                <div class="col-12 tm-block-col">
                    <div class="tm-bg-primary-dark tm-block tm-block-taller tm-block-scroll">
                         <p align="center"><b>BUSINESS PERFOMANCE REPORT</b></p>
                        <table class="table">
                            <thead>
                                <tr><th scope="col">No</th>
                                     <th scope="col">Tin number</th>
                                    <th scope="col">Business Name</th>
                                    <th scope="col">Business Categories</th>
                                    <th scope="col">Business Location</th>
                                    <th scope="col">CATEGORIES</th>
                                    <th scope="col">COMMENT</th>
                                    <th scope="col">GRADE</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                                      <?php 
                                    include('config.php');
                                    $sql = "SELECT * FROM business,credidential WHERE business.credidential_id=credidential.credidential_id AND  business_category='Hotels' AND business.status='Desactive'";
                                    $query = $conn->prepare($sql);
                                    $query->execute();
                                    $results=$query->fetchAll(PDO::FETCH_OBJ);
                                    $cnt=1;
                                    if($query->rowCount() > 0)
                                    {
                                    foreach($results as $result)
                                    {   
                                        ?>  
                                <tr>
                                    <th scope="row"><b><?php echo $cnt;?></b></th>
                                    <td>

                                            </div><?php echo  htmlentities($result->username);?>
                                    </td>
                                        
                                    </td>
                                    <td><b><?php echo htmlentities($result->tin_number);?></b></td>
                                    <td><b><?php echo htmlentities($result->business_name);?></b></td>
                                    <td><b><?php echo htmlentities($result->business_location);?></b></td>
                                    <td><?php echo htmlentities($result->status);?></td>
                                    
                                </tr>    
                                    <?php $cnt=$cnt+1;}} ?>                      
                            </tbody>

                        </table>
                        
                        <h2 align="right" style="size: 20px;"><a class="btn btn-success btn-small" href="javascript:window.print()">Print</a></h2>
                    </div>

                </div>
            </div>
        </div>
        <!--================Blog Area =================-->
        <!-- footer part start-->
        <footer class="footer-area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-3 col-sm-6 col-md-4">
                        <div class="single-footer-widget footer_1">
                            <h4>About Us</h4>
                            <p>Heaven fruitful doesn't over for these theheaven fruitful doe over  days 
                                appear creeping seasons sad behold beari ath of it fly signs bearing 
                            be one blessed after.</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-sm-6 col-md-4">
                        <div class="single-footer-widget footer_2">
                            <h4>Important Link</h4>
                            <div class="contact_info">
                                <ul>
                                    <li><a href="#">WHMCS-bridge</a></li>
                                    <li><a href="#"> Search Domain</a></li>
                                    <li><a href="#">My Account</a></li>
                                    <li><a href="#">Shopping Cart</a></li>
                                    <li><a href="#"> Our Shop</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-sm-6 col-md-4">
                        <div class="single-footer-widget footer_2">
                            <h4>Contact us</h4>
                            <div class="contact_info">
                                <p><span> Address :</span>Hath of it fly signs bear be one blessed after </p>
                                <p><span> Phone :</span> +2 36 265 (8060)</p>
                                <p><span> Email : </span>info@colorlib.com </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-sm-8 col-md-6">
                        <div class="single-footer-widget footer_3">
                            <h4>Newsletter</h4>
                            <p>Heaven fruitful doesn't over lesser in days. Appear creeping seas</p>
                            <form action="#">
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" placeholder='Email Address'
                                        onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address'">
                                        <div class="input-group-append">
                                            <button class="btn" type="button"><i class="fas fa-paper-plane"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="copyright_part_text">
                    <div class="row">
                        <div class="col-lg-8">
                            <p class="footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                Copyright &copy;Shyaka Gen All rights reserved | This template is made with <i class="ti-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Esther UMUBYEYI</a>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                            </div>
                            <div class="col-lg-4">
                                <div class="copyright_social_icon text-right">
                                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#"><i class="ti-dribbble"></i></a>
                                    <a href="#"><i class="fab fa-behance"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- footer part end-->
        <!-- jquery plugins here-->
        <!-- jquery -->
        <script src="js/jquery-1.12.1.min.js"></script>
        <!-- popper js -->
        <script src="js/popper.min.js"></script>
        <!-- bootstrap js -->
        <script src="js/bootstrap.min.js"></script>
        <!-- easing js -->
        <script src="js/jquery.magnific-popup.js"></script>
        <!-- swiper js -->
        <script src="js/swiper.min.js"></script>
        <!-- swiper js -->
        <script src="js/masonry.pkgd.js"></script>
        <!-- particles js -->
        <script src="js/owl.carousel.min.js"></script>
        <!-- swiper js -->
        <script src="js/slick.min.js"></script>
        <script src="js/gijgo.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <!-- custom js -->
        <script src="js/custom.js"></script>
    </body>

    </html>
