<?php
session_start();
error_reporting();
include('config.php');

//check if the form has submit

if(isset($_POST['submit'])){

    //RECEIVE THE VALUE FROM VARIABLE

    $identity_number    = htmlentities($_POST['identity_number']);
    $first_name         = htmlentities($_POST['first_name']);
    $last_name          = htmlentities($_POST['last_name']);
    $email_address      = htmlentities($_POST['email_address']);
    $telephone_number   = htmlentities($_POST['telephone_number']);
    $person_id          = htmlentities($_POST['person_id']);

    //SQL QUERY THE FOR INSERTING 

    $sql   = "UPDATE person SET identity_number=:identity_number,first_name=:first_name,last_name=:last_name,email_address=:email_address,telephone_number=:telephone_number WHERE person_id=:person_id";

    $query = $conn->prepare($sql);
    $query->bindParam(':identity_number',$identity_number,PDO::PARAM_STR);
    $query->bindParam(':first_name',$first_name,PDO::PARAM_STR);
    $query->bindParam(':last_name',$last_name,PDO::PARAM_STR);
    $query->bindParam(':email_address',$email_address,PDO::PARAM_STR);
    $query->bindParam(':telephone_number',$telephone_number,PDO::PARAM_STR);
    $query->execute();

 //check if the query has success

    if($query){
        $msg = "Data successful Saved";
    }else{
        $errors = "Failed to saved something goes wrong";
    }
}


?>
<!doctype html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Weconnect</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="icon" href="logo.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/gijgo.min.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/all.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="css/style.css">

    <link rel="stylesheet" type="text/css" href="css/styless.css" media="all" />
    <link rel="stylesheet" type="text/css" href="css/demo.css" media="all" />
    <!-- jQuery -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>

    <!-- FlexSlider -->
    <script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" charset="utf-8">
        var $ = jQuery.noConflict();
        $(window).load(function() {
            $('.flexslider').flexslider({
              animation: "fade"
          });

            $(function() {
                $('.show_menu').click(function(){
                    $('.menu').fadeIn();
                    $('.show_menu').fadeOut();
                    $('.hide_menu').fadeIn();
                });
                $('.hide_menu').click(function(){
                    $('.menu').fadeOut();
                    $('.show_menu').fadeIn();
                    $('.hide_menu').fadeOut();
                });
            });
        });
    </script>
</head>
<body>
    <?php include('admin_header.php');?>
    <!-- Header part end-->
   <div class="container">
            <div class="row">
                <div class="col">
                    <p class="text-white mt-5 mb-5"></b></p>
                </div>
            </div>
            <!-- row -->
            <div class="row tm-content-row">
                <div class="col-12 tm-block-col">
                    <div class="tm-bg-primary-dark tm-block tm-block-taller tm-block-scroll">
                         <p align="center"><b>Available Business Owner</b></p>
                     </br></br>
                        <table class="table">
                            <thead>
                                <tr><th scope="col">No</th>
                                    <th scope="col">Identity Number</th>
                                    <th scope="col">First Name</th>
                                    <th scope="col">Last Name</th>
                                    <th scope="col">Email Address</th>
                                    <th scope="col">Phone Number</th>
                                    <th scope="col">Action<th>
                                </tr>
                            </thead>
                            <tbody>
                                      <?php 
                                    //include('config.php');
                                    $sql = "SELECT * FROM person ";
                                    $query = $conn->prepare($sql);
                                    $query->execute();
                                    $results=$query->fetchAll(PDO::FETCH_OBJ);
                                    $cnt=1;
                                    if($query->rowCount() > 0)
                                    {
                                    foreach($results as $result)
                                    {   
                                        ?>  
                                <tr>
                                    <th scope="row"><b><?php echo $cnt;?></b></th>
                                    <td>
                                        <div class="tm-status-circle moving">
                                        </div><?php echo  htmlentities($result->identity_number);?>
                                    </td>
                                    <td><b><?php echo htmlentities($result->first_name);?></b></td>
                                    <td><b><?php echo htmlentities($result->last_name);?></b></td>
                                    <td><?php echo htmlentities($result->email_address);?></td>
                                    <td><?php echo htmlentities($result->telephone_number);?></td>
                                    <td><h5><a class="btn btn-success btn-small" href="admin_update_owner.php?person_id=<?php echo htmlentities($result->person_id);?>" style="size:5px">Update</a>  <a class="btn btn-success btn-small" href="admin_registering_business.php?person_id=<?php echo htmlentities($result->person_id);?>" style="size:5px">Regist Business</a></h4></td>
                                </tr>    
                                    <?php $cnt=$cnt+1;}} ?>                      
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!--================Blog Area =================-->
        <!-- footer part start-->
      <?php include('footer.php');?>
        <!-- footer part end-->
        <!-- jquery plugins here-->
        <!-- jquery -->
        <script src="js/jquery-1.12.1.min.js"></script>
        <!-- popper js -->
        <script src="js/popper.min.js"></script>
        <!-- bootstrap js -->
        <script src="js/bootstrap.min.js"></script>
        <!-- easing js -->
        <script src="js/jquery.magnific-popup.js"></script>
        <!-- swiper js -->
        <script src="js/swiper.min.js"></script>
        <!-- swiper js -->
        <script src="js/masonry.pkgd.js"></script>
        <!-- particles js -->
        <script src="js/owl.carousel.min.js"></script>
        <!-- swiper js -->
        <script src="js/slick.min.js"></script>
        <script src="js/gijgo.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <!-- custom js -->
        <script src="js/custom.js"></script>
    </body>

    </html>
