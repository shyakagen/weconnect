<?php
session_start();
error_reporting();
include('config.php');

//check if the form has submit
$msg="";
$error="";

if(isset($_POST['submit'])){

    //RECEIVE THE VALUE FROM VARIABLE

    $identity_number    = htmlentities($_POST['identity_number']);
    $first_name         = htmlentities($_POST['first_name']);
    $last_name          = htmlentities($_POST['last_name']);
    $email_address      = htmlentities($_POST['email_address']);
    $telephone_number   = htmlentities($_POST['telephone_number']);

    //SQL QUERY THE FOR INSERTING 

    $check = $conn->prepare("SELECT  identity_number FROM person WHERE  identity_number=:identity_number ");
    $check->bindValue(':identity_number', $_POST['identity_number']);
    $check->execute();
    $result = $check->fetch();
    if(!$result){

         $sql   = "INSERT INTO person(identity_number,first_name,last_name,email_address,telephone_number) VALUES(:identity_number,:first_name,:last_name,:email_address,:telephone_number)";

    $query = $conn->prepare($sql);
    $query->bindParam(':identity_number',$identity_number,PDO::PARAM_STR);
    $query->bindParam(':first_name',$first_name,PDO::PARAM_STR);
    $query->bindParam(':last_name',$last_name,PDO::PARAM_STR);
    $query->bindParam(':email_address',$email_address,PDO::PARAM_STR);
    $query->bindParam(':telephone_number',$telephone_number,PDO::PARAM_STR);
    $query->execute();

    if($query){
        $msg = "Data successful Saved";
    }
    else{
        $error = "Failed to saved something goes wrong";
    }

    }
}


?>
<!doctype html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Weconnect</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="icon" href="logo.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/gijgo.min.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/all.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="css/style.css">

    <link rel="stylesheet" type="text/css" href="css/styless.css" media="all" />
    <link rel="stylesheet" type="text/css" href="css/demo.css" media="all" />
    <!-- jQuery -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>

    <!-- FlexSlider -->
    <script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" charset="utf-8">
        var $ = jQuery.noConflict();
        $(window).load(function() {
            $('.flexslider').flexslider({
              animation: "fade"
          });

            $(function() {
                $('.show_menu').click(function(){
                    $('.menu').fadeIn();
                    $('.show_menu').fadeOut();
                    $('.hide_menu').fadeIn();
                });
                $('.hide_menu').click(function(){
                    $('.menu').fadeOut();
                    $('.show_menu').fadeIn();
                    $('.hide_menu').fadeOut();
                });
            });
        });
    </script>
</head>
<body>
    <?php include('admin_header.php');?>
    <!-- Header part end-->
    <!--================Blog Area =================-->
    <section class="blog_area section_padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="blog_right_sidebar">
                        <p align="center"><b>User Business Registration Form</b></p>
                        <aside class="single_sidebar_widget search_widget">
                            <?php if($msg){?>
                                <div class="alert alert-success left-icon-alert" role="alert">
                                   <strong>Well done!</strong><?php echo htmlentities($msg); ?>
                                   </div><?php } 
                                   else if($error){?>
                                    <div class="alert alert-danger left-icon-alert" role="alert">
                                        <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                                    </div>
                                <?php } ?>

                                <form method="POST" name="myForm" action="<?php echo $_SERVER['PHP_SELF'];?>" onsubmit="return validateFormOnSubmit()">

                                    <div class="form-group row">
                                         <label for="identity_number" class="col-md-4 col-form-label text-md-right">Identity number</label>

                                        <div class="col-md-8">
                                            <input id="identity_number" type="text" class="form-control"  name="identity_number" maxlength="16" onkeyup="validateId(this.value)" type="text" class="form-control"required="">
                                             <p><span style="color:red" id="identity-error"></span></p>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label for="first_name" class="col-md-4 col-form-label text-md-right">First name</label>

                                        <div class="col-md-8">
                                            <input id="first_name" onkeyup="validateFirstname(this.value)" type="text" class="form-control" name="first_name" required="" >
                                             <p><span style="color:red" id="first_name-error"></span></p>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label for="last_name" class="col-md-4 col-form-label text-md-right">Last name</label>

                                        <div class="col-md-8">
                                            <input id="last_name" onkeyup="validateLastname(this.value)" type="text" class="form-control " name="last_name" required="">
                                             <p><span style="color:red" id="last_name-error"></span></p>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="email_address" class="col-md-4 col-form-label text-md-right">Email Address</label>

                                        <div class="col-md-8">
                                            <input id="email_address" type="email" class="form-control " name="email_address"  required="">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="telephone_number" class="col-md-4 col-form-label text-md-right">Telephone number</label>

                                        <div class="col-md-8">
                                            <input id="telephone_number" maxlength="10" onkeyup="validatePhone(this.value)" type="text" class="form-control " name="telephone_number" required="">
                                             <p><span style="color:red" id="phone-error"></span></p>
                                        </div>
                                    </div>

                                    
                                    <button type="submit" class="button rounded-0 primary-bg text-white w-100 btn_4" name="submit">
                                        Register Owner Business
                                    </button>
                                </form>
                            </aside>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="blog_right_sidebar">
                            <aside class="single_sidebar_widget search_widget">
                                <form action="#">
                                    <div class="form-group">
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" placeholder='Search Keyword'
                                            onfocus="this.placeholder = ''"
                                            onblur="this.placeholder = 'Search Keyword'">
                                            <div class="input-group-append">
                                                <button class="btn" type="button"><i class="ti-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="button rounded-0 primary-bg text-white w-100 btn_4"
                                    type="submit">Search</button>
                                </form>
                            </aside>

                            <aside class="single_sidebar_widget post_category_widget">
                                 <h4 class="widget_title">Our Business Category</h4>
                                <ul class="list cat-list">
                                    <li>
                                        <a href="#" class="d-flex">
                                            <p>Resaurant</p>
                                          
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="d-flex">
                                            <p>Hotel</p>
                                          
                                        </a>
                                    </li>
                                </ul>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================Blog Area =================-->
        <!-- footer part start-->
       <?php include('footer.php');?>
        <!-- footer part end-->
        <!-- jquery plugins here-->
        <!-- jquery -->
        <script type="text/javascript">


           //FORM VALIDATION FUNCTION

            function validateFirstname(){
              var first_name = document.myForm.first_name.value;
              var msg = ''
              if(!isNaN(first_name)){

                msg='Please First name must be a text';
                document.myForm.first_name.focus() ;
                document.getElementById('first_name-error').innerHTML = msg;
                return false;
              }
              else{
                return (true);
              }
            }

             //FORM VALIDATION FUNCTION

            function validateLastname(){
              var last_name = document.myForm.last_name.value;
              var msg = ''
              if(!isNaN(last_name)){

                msg='Please Last name must be a text';
                document.myForm.last_name.focus() ;
                document.getElementById('last_name-error').innerHTML = msg;
                return false;
              }
              else{
                return (true);
              }
            }


            //FORM VALIDATION FUNCTION

            function validateFormOnSubmit(){
              var first_name = document.myForm.first_name.value;
              var msg = ''
              if(!isNaN(first_name)){

                msg='Please Username must be a text';
                document.myForm.first_name.focus() ;
                document.getElementById('first_name-error').innerHTML = msg;
                return false;
              }
              else{
                return (true);
              }
            }

            //PHONE VALIDATION FUNCTION

function validatePhone(str)
    {
        var msg = '';
        var is_valid = false;
        if(str === ''){
            msg = 'Phone number is required';
        }
        else{
        if(str.length !== 10){
            msg = 'Phone number must be 10 digits';
        }
        else{
            const rwanda_phones = ['078', '073', '072'];
            var test_value = str.substring(0,3);
            for(let phone of rwanda_phones)
            {
                if(test_value === phone){
                    is_valid = true;
                    console.log("found");
                    break;
                }
            }
            if( ! is_valid)
                msg = 'Invalid Rwandan Phone number';
            else
                msg = '';
        }
        }
        document.getElementById('phone-error').innerHTML = msg;
        return is_valid;
  }

    //VALIDATION OF NATIONAL ID

    function validateId(str)
    {
        var msg = '';
        var is_valid = false;
        if(str === ''){
            msg = 'Identity number is required';
        }
        else{
        if(str.length !== 16){
            msg = 'Identity number is must have 16 digits';
        }
        else{
            const id_number = ['119'];
            var test_value = str.substring(0,3);
            for(let id of id_number)
            {
                if(test_value <= id && test_value !=='000'){
                    is_valid = true;
                   msg = 'Rwandan Identity number is validated';
                }
                else {
                     msg = 'We are not yet in that Year';
                }
                
            }

        }

        }
        document.getElementById('identity-error').innerHTML = msg;
        return is_valid;
  }
  


        </script>
        <script src="js/jquery-1.12.1.min.js"></script>
        <!-- popper js -->
        <script src="js/popper.min.js"></script>
        <!-- bootstrap js -->
        <script src="js/bootstrap.min.js"></script>
        <!-- easing js -->
        <script src="js/jquery.magnific-popup.js"></script>
        <!-- swiper js -->
        <script src="js/swiper.min.js"></script>
        <!-- swiper js -->
        <script src="js/masonry.pkgd.js"></script>
        <!-- particles js -->
        <script src="js/owl.carousel.min.js"></script>
        <!-- swiper js -->
        <script src="js/slick.min.js"></script>
        <script src="js/gijgo.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <!-- custom js -->
        <script src="js/custom.js"></script>
    </body>

    </html>
