<?php
session_start();
error_reporting();
include('config.php');

//check if the form has submit

if(isset($_POST['submit'])){

    //RECEIVE THE VALUE FROM VARIABLE

    $identity_number    = htmlentities($_POST['identity_number']);
    $first_name         = htmlentities($_POST['first_name']);
    $last_name          = htmlentities($_POST['last_name']);
    $email_address      = htmlentities($_POST['email_address']);
    $telephone_number   = htmlentities($_POST['telephone_number']);
    $person_id          = htmlentities($_POST['person_id']);

    //SQL QUERY THE FOR INSERTING 

    $sql   = "UPDATE person SET identity_number=:identity_number,first_name=:first_name,last_name=:last_name,email_address=:email_address,telephone_number=:telephone_number WHERE person_id=:person_id";

    $query = $conn->prepare($sql);
    $query->bindParam(':identity_number',$identity_number,PDO::PARAM_STR);
    $query->bindParam(':first_name',$first_name,PDO::PARAM_STR);
    $query->bindParam(':last_name',$last_name,PDO::PARAM_STR);
    $query->bindParam(':email_address',$email_address,PDO::PARAM_STR);
    $query->bindParam(':telephone_number',$telephone_number,PDO::PARAM_STR);
    $query->bindParam(':person_id',$person_id,PDO::PARAM_STR);

    $query->execute();

 //check if the query has success

    if($query){
        $msg = "Data successful Saved";
    }else{
        $errors = "Failed to saved something goes wrong";
    }
}


?>
<!doctype html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Weconnect</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="icon" href="/images/frontend_images/logo.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- animate CSS -->
    <link rel="stylesheet" href="css/animate.css">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- themify CSS -->
    <link rel="stylesheet" href="css/themify-icons.css">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="css/flaticon.css">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/gijgo.min.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/all.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="css/style.css">

    <link rel="stylesheet" type="text/css" href="css/styless.css" media="all" />
    <link rel="stylesheet" type="text/css" href="css/demo.css" media="all" />
    <!-- jQuery -->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>

    <!-- FlexSlider -->
    <script type="text/javascript" src="js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" charset="utf-8">
        var $ = jQuery.noConflict();
        $(window).load(function() {
            $('.flexslider').flexslider({
              animation: "fade"
          });

            $(function() {
                $('.show_menu').click(function(){
                    $('.menu').fadeIn();
                    $('.show_menu').fadeOut();
                    $('.hide_menu').fadeIn();
                });
                $('.hide_menu').click(function(){
                    $('.menu').fadeOut();
                    $('.show_menu').fadeIn();
                    $('.hide_menu').fadeOut();
                });
            });
        });
    </script>
</head>
<body>
    <?php include('admin_header.php');?>
    <!-- Header part end-->
    <!--================Blog Area =================-->
    <section class="blog_area section_padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="blog_right_sidebar">
                         <p align="center"><b>User Business Update Form</b></p>
                        <aside class="single_sidebar_widget search_widget">
                            <?php if($msg){?>
                                <div class="alert alert-success left-icon-alert" role="alert">
                                   <strong>Well done!</strong><?php echo htmlentities($msg); ?>
                                   </div><?php } 
                                   else if($error){?>
                                    <div class="alert alert-danger left-icon-alert" role="alert">
                                        <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                                    </div>
                                <?php } ?>
                                <form method="POST" action="<?php echo $_SERVER['PHP_SELF'];?>">
                                    <?php 
                                    include('config.php');
                                    $person_id = $_GET['person_id'];
                                    $sql = "SELECT * FROM person WHERE person_id=:person_id";                                        ;
                                    $query = $conn->prepare($sql);
                                    $query->bindParam(':person_id',$person_id,PDO::PARAM_STR);
                                    $query->execute();
                                    $results=$query->fetchAll(PDO::FETCH_OBJ);
                                    $cnt=1;
                                    if($query->rowCount() > 0)
                                    {
                                    foreach($results as $result)
                                    {   
                                        ?>  
                                        <div class="form-group row">
                                        <label for="person_id" class="col-md-4 col-form-label text-md-right"></label>
                                        <div class="col-md-8">
                                            <input id="person_id" type="hidden" class="form-control" value="<?php echo htmlentities($result->person_id);?>" name="person_id" value="" required autocomplete="off">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="identity_number" class="col-md-4 col-form-label text-md-right">Identity number</label>
                                        <div class="col-md-8">
                                            <input id="identity_number" type="text" class="form-control" name="identity_number" value="<?php echo htmlentities($result->identity_number);?>" required autocomplete="off">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="first_name" class="col-md-4 col-form-label text-md-right">First name</label>

                                        <div class="col-md-8">
                                            <input id="first_name" type="text" class="form-control " name="first_name" value="<?php echo htmlentities($result->first_name);?>" required autocomplete="off">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="last_name" class="col-md-4 col-form-label text-md-right">Last name</label>

                                        <div class="col-md-8">
                                            <input id="last_name" type="text" class="form-control " name="last_name" value="<?php echo htmlentities($result->last_name);?>" required autocomplete="off">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="email_address" class="col-md-4 col-form-label text-md-right">Email Address</label>

                                        <div class="col-md-8">
                                            <input id="email_address" type="text" class="form-control " name="email_address" value="<?php echo htmlentities($result->email_address);?>" required autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="telephone_number" class="col-md-4 col-form-label text-md-right">Telephone number</label>

                                        <div class="col-md-8">
                                            <input id="telephone_number" type="text" class="form-control " name="telephone_number" value="<?php echo htmlentities($result->telephone_number);?>" required autocomplete="off">
                                        </div>
                                    </div>
                                    <button type="submit" class="button rounded-0 primary-bg text-white w-100 btn_4" name="submit">
                                        Update Owner Business
                                    </button>
                                </form>
                                 <?php }} ?>
                            </aside>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="blog_right_sidebar">
                            <aside class="single_sidebar_widget search_widget">
                                <form action="#">
                                    <div class="form-group">
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" placeholder='Search Keyword'
                                            onfocus="this.placeholder = ''"
                                            onblur="this.placeholder = 'Search Keyword'">
                                            <div class="input-group-append">
                                                <button class="btn" type="button"><i class="ti-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="button rounded-0 primary-bg text-white w-100 btn_4"
                                    type="submit">Search</button>
                                </form>
                            </aside>

                            <aside class="single_sidebar_widget post_category_widget">
                                <h4 class="widget_title">Our Business Category</h4>
                                <ul class="list cat-list">
                                    <li>
                                        <a href="#" class="d-flex">
                                            <p>Resaurant</p>
                                          
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="d-flex">
                                            <p>Hotel</p>
                                           
                                        </a>
                                    </li>
                                </ul>
                            </aside>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================Blog Area =================-->
        <!-- footer part start-->
        <footer class="footer-area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-3 col-sm-6 col-md-4">
                        <div class="single-footer-widget footer_1">
                            <h4>About Us</h4>
                            <p>Heaven fruitful doesn't over for these theheaven fruitful doe over  days 
                                appear creeping seasons sad behold beari ath of it fly signs bearing 
                            be one blessed after.</p>
                        </div>
                    </div>
                    <div class="col-xl-3 col-sm-6 col-md-4">
                        <div class="single-footer-widget footer_2">
                            <h4>Important Link</h4>
                            <div class="contact_info">
                                <ul>
                                    <li><a href="#">WHMCS-bridge</a></li>
                                    <li><a href="#"> Search Domain</a></li>
                                    <li><a href="#">My Account</a></li>
                                    <li><a href="#">Shopping Cart</a></li>
                                    <li><a href="#"> Our Shop</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-sm-6 col-md-4">
                        <div class="single-footer-widget footer_2">
                            <h4>Contact us</h4>
                            <div class="contact_info">
                                <p><span> Address :</span>Hath of it fly signs bear be one blessed after </p>
                                <p><span> Phone :</span> +2 36 265 (8060)</p>
                                <p><span> Email : </span>info@colorlib.com </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-sm-8 col-md-6">
                        <div class="single-footer-widget footer_3">
                            <h4>Newsletter</h4>
                            <p>Heaven fruitful doesn't over lesser in days. Appear creeping seas</p>
                            <form action="#">
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" placeholder='Email Address'
                                        onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address'">
                                        <div class="input-group-append">
                                            <button class="btn" type="button"><i class="fas fa-paper-plane"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="copyright_part_text">
                    <div class="row">
                        <div class="col-lg-8">
                            <p class="footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                Copyright &copy;Shyaka Gen All rights reserved | This template is made with <i class="ti-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Esther UMUBYEYI</a>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                            </div>
                            <div class="col-lg-4">
                                <div class="copyright_social_icon text-right">
                                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#"><i class="ti-dribbble"></i></a>
                                    <a href="#"><i class="fab fa-behance"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- footer part end-->
        <!-- jquery plugins here-->
        <!-- jquery -->
        <script src="js/jquery-1.12.1.min.js"></script>
        <!-- popper js -->
        <script src="js/popper.min.js"></script>
        <!-- bootstrap js -->
        <script src="js/bootstrap.min.js"></script>
        <!-- easing js -->
        <script src="js/jquery.magnific-popup.js"></script>
        <!-- swiper js -->
        <script src="js/swiper.min.js"></script>
        <!-- swiper js -->
        <script src="js/masonry.pkgd.js"></script>
        <!-- particles js -->
        <script src="js/owl.carousel.min.js"></script>
        <!-- swiper js -->
        <script src="js/slick.min.js"></script>
        <script src="js/gijgo.min.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <!-- custom js -->
        <script src="js/custom.js"></script>
    </body>

    </html>
